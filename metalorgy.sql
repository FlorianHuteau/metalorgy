-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 14 jan. 2022 à 22:45
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `metalorgy`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `idAdmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`idAdmin`) VALUES
(13);

-- --------------------------------------------------------

--
-- Structure de la table `artiste`
--

CREATE TABLE `artiste` (
  `idArtiste` int(255) NOT NULL,
  `NomArtiste` varchar(50) NOT NULL,
  `DescriptionArtiste` text NOT NULL,
  `AffichageArtiste` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `artiste`
--

INSERT INTO `artiste` (`idArtiste`, `NomArtiste`, `DescriptionArtiste`, `AffichageArtiste`) VALUES
(2, 'Slipknot', 'Le meilleur groupe du monde', 0),
(3, 'Slayeur', 'Groupe de Thrash Metal, membre du Big Four of Thrash Metal', 0),
(4, 'Pogo Car Crash Control', 'Groupe français de Punk Hardcore', 0),
(5, 'Loudblast', 'Groupe français de Death Metal', 0),
(6, 'Kadinja', 'Groupe français de Metal Progressif', 0),
(7, 'Sidilarsen', 'Groupe français de Metal Industriel', 0),
(8, 'Acod', 'Groupe français de Death Metal', 0),
(9, 'Bloody Alchemy', 'Groupe français de Death Metal', 0),
(10, 'Overstrange Mood', 'Groupe français de Metal Progressif', 0),
(11, 'Causality', 'Groupe français de Djent', 0),
(12, 'Chabtan', 'Groupe français de Death Metal', 0),
(13, 'Dysfunctional', 'Groupe français de Death Metal', 0),
(14, 'Ghost Anthem', 'Groupe français de Metalcore ', 0),
(15, 'Nothing But Echoes', 'Groupe français de Metal Progressif', 0),
(16, 'Red Dawn', 'Groupe français de Death Metal', 0),
(17, 'Sick Sad World', 'Groupe français de Post-Metal', 0),
(18, 'Whisper Night', 'Groupe français de Melodeath/Metalcore', 0),
(19, 'Worms Eat Her', 'Groupe français de Death Metal', 0),
(20, 'Gojira', 'Groupe français de Death Metal', 0),
(21, 'Megadeth', 'Groupe de Thrash Metal membre du Big Four of Thrash Metal', 0),
(24, 'Metalica', 'Groupe de Thrash Metal membre du Big Four of Thrash', 0);

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE `event` (
  `idEvent` int(255) NOT NULL,
  `NomEvent` varchar(50) NOT NULL,
  `DateDebEvent` datetime NOT NULL,
  `DateFinEvent` datetime NOT NULL,
  `DescriptionEvent` text NOT NULL,
  `AdresseEvent` varchar(50) NOT NULL,
  `AffichageEvent` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `event`
--

INSERT INTO `event` (`idEvent`, `NomEvent`, `DateDebEvent`, `DateFinEvent`, `DescriptionEvent`, `AdresseEvent`, `AffichageEvent`) VALUES
(2, 'Hellfest', '2021-11-30 18:17:29', '2021-12-02 18:17:43', 'Plus grand festival musiques extrêmes', 'Clisson', 0),
(3, 'Nantes Metal Fest', '2021-12-02 20:00:00', '2021-12-04 23:59:00', 'Festival de Metal au Ferailleur de Nantes', '21 Quai des Antilles, 44200 Nantes', 0);

-- --------------------------------------------------------

--
-- Structure de la table `programmation`
--

CREATE TABLE `programmation` (
  `id_Event` int(11) NOT NULL,
  `id_Artiste` int(11) NOT NULL,
  `NProg` int(11) DEFAULT NULL,
  `HeureProg` time NOT NULL,
  `id_Scene` int(11) NOT NULL,
  `DateProg` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `programmation`
--

INSERT INTO `programmation` (`id_Event`, `id_Artiste`, `NProg`, `HeureProg`, `id_Scene`, `DateProg`) VALUES
(2, 3, 4, '21:30:00', 1, '2021-11-30'),
(2, 2, 4, '20:00:00', 1, '2021-11-30'),
(2, 2, 4, '20:00:00', 1, '2021-11-30'),
(2, 4, NULL, '22:00:00', 1, '2021-12-01'),
(2, 5, NULL, '20:00:00', 1, '2021-12-02'),
(3, 6, NULL, '23:00:00', 8, '2021-12-03'),
(3, 16, NULL, '22:00:00', 8, '2021-12-02'),
(3, 7, NULL, '23:00:00', 8, '2021-12-04'),
(3, 12, NULL, '22:00:00', 8, '2021-12-03'),
(3, 14, NULL, '21:00:00', 8, '2021-12-03'),
(3, 13, NULL, '21:00:00', 8, '2021-12-02');

-- --------------------------------------------------------

--
-- Structure de la table `scene`
--

CREATE TABLE `scene` (
  `idScene` int(11) NOT NULL,
  `NomScene` varchar(50) NOT NULL,
  `NScene` int(11) NOT NULL,
  `id_Event` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `scene`
--

INSERT INTO `scene` (`idScene`, `NomScene`, `NScene`, `id_Event`) VALUES
(1, 'Mainstage 1', 1, 2),
(2, 'Mainstage 2', 2, 2),
(3, 'Warzone', 3, 2),
(4, 'Valley', 4, 2),
(5, 'Altar', 5, 2),
(7, 'Temple', 6, 2),
(8, 'Scène 1', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `stylemusique`
--

CREATE TABLE `stylemusique` (
  `idStyle` int(11) NOT NULL,
  `NomStyle` varchar(50) NOT NULL,
  `DescriptionStyle` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `stylemusique`
--

INSERT INTO `stylemusique` (`idStyle`, `NomStyle`, `DescriptionStyle`) VALUES
(1, 'Thrash Metal', ' Forme de musique hard rock, considérée comme un mélange de punk et de heavy metal, caractérisé par l’alliance de la lourdeur du heavy metal et de la vitesse du punk.'),
(2, 'Death Metal', 'Sous-genre de heavy metal, issu du thrash metal, qui se distingue par un chant hurlé utilisant essentiellement le grunt, une rythmique très rapide et des thèmes de chanson centrés sur la violence.'),
(3, 'Différent style', 'Différent style de Metal à gérer plus tard'),
(5, 'Black Metal', ' issu du genre thrash metal et né dans les années 1990, caractérisé par un chant hurlé poussé dans les aigus, l’utilisation massive des blast beats au sein d’une rythmique très rapide, des guitares très distordues, des thèmes centrés sur l’occultisme, le paganisme et la misanthropie.'),
(6, 'Punk Hardcore', 'Le punk hardcore, souvent simplifié en hardcore, est un sous-genre du punk rock apparu en Amérique du Nord à la fin des années 1970. Il est considéré comme la première vague d\'artistes de punks radicalisés et engagés, devenant plus underground ou inconditionnels, accélérant les tempos et raccourcissant les morceaux, notamment en réaction à la commercialisation de certains artistes punk. Le son est plus lourd et les mélodies plus rapides que le style de punk rock des années 1970. Il est caractérisé par des chansons courtes, souvent empreintes d\'un message fort.');

-- --------------------------------------------------------

--
-- Structure de la table `style_artiste_event`
--

CREATE TABLE `style_artiste_event` (
  `id_Style` int(11) NOT NULL,
  `id_Artiste` int(11) DEFAULT NULL,
  `id_Event` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `style_artiste_event`
--

INSERT INTO `style_artiste_event` (`id_Style`, `id_Artiste`, `id_Event`) VALUES
(2, 20, NULL),
(1, 24, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `idUser` int(255) NOT NULL,
  `NomUser` varchar(50) NOT NULL,
  `PrenomUser` varchar(50) NOT NULL,
  `MailUser` varchar(50) NOT NULL,
  `PasswordUser` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`idUser`, `NomUser`, `PrenomUser`, `MailUser`, `PasswordUser`) VALUES
(13, 'admin', 'admin', 'admin@admin.admin', '6Ae9zKajPVKAwFdyfzHEQw=='),
(21, 'user', 'user', 'user@user.user', '9J4cuf1t/dbFtm4XcB1NKw==');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD KEY `admin_ibfk_User` (`idAdmin`);

--
-- Index pour la table `artiste`
--
ALTER TABLE `artiste`
  ADD PRIMARY KEY (`idArtiste`);

--
-- Index pour la table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`idEvent`);

--
-- Index pour la table `programmation`
--
ALTER TABLE `programmation`
  ADD KEY `programmation_ibfk_Artiste` (`id_Artiste`),
  ADD KEY `programmation_ibfk_Event` (`id_Event`),
  ADD KEY `id_Scene` (`id_Scene`);

--
-- Index pour la table `scene`
--
ALTER TABLE `scene`
  ADD PRIMARY KEY (`idScene`),
  ADD KEY `id_Event` (`id_Event`);

--
-- Index pour la table `stylemusique`
--
ALTER TABLE `stylemusique`
  ADD PRIMARY KEY (`idStyle`);

--
-- Index pour la table `style_artiste_event`
--
ALTER TABLE `style_artiste_event`
  ADD KEY `style_artiste_event_ibfk_Artiste` (`id_Artiste`),
  ADD KEY `style_artiste_event_ibfk_Event` (`id_Event`),
  ADD KEY `style_artiste_event_ibfk_Style` (`id_Style`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `artiste`
--
ALTER TABLE `artiste`
  MODIFY `idArtiste` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `event`
--
ALTER TABLE `event`
  MODIFY `idEvent` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `scene`
--
ALTER TABLE `scene`
  MODIFY `idScene` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `stylemusique`
--
ALTER TABLE `stylemusique`
  MODIFY `idStyle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_User` FOREIGN KEY (`idAdmin`) REFERENCES `user` (`idUser`);

--
-- Contraintes pour la table `programmation`
--
ALTER TABLE `programmation`
  ADD CONSTRAINT `programmation_ibfk_1` FOREIGN KEY (`id_Scene`) REFERENCES `scene` (`idScene`),
  ADD CONSTRAINT `programmation_ibfk_Artiste` FOREIGN KEY (`id_Artiste`) REFERENCES `artiste` (`idArtiste`),
  ADD CONSTRAINT `programmation_ibfk_Event` FOREIGN KEY (`id_Event`) REFERENCES `event` (`idEvent`);

--
-- Contraintes pour la table `scene`
--
ALTER TABLE `scene`
  ADD CONSTRAINT `scene_ibfk_1` FOREIGN KEY (`id_Event`) REFERENCES `event` (`idEvent`);

--
-- Contraintes pour la table `style_artiste_event`
--
ALTER TABLE `style_artiste_event`
  ADD CONSTRAINT `style_artiste_event_ibfk_Artiste` FOREIGN KEY (`id_Artiste`) REFERENCES `artiste` (`idArtiste`),
  ADD CONSTRAINT `style_artiste_event_ibfk_Event` FOREIGN KEY (`id_Event`) REFERENCES `event` (`idEvent`),
  ADD CONSTRAINT `style_artiste_event_ibfk_Style` FOREIGN KEY (`id_Style`) REFERENCES `stylemusique` (`idStyle`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
