import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metalorgy/Services/api_service.dart';
import 'package:metalorgy/main.dart';
import 'package:metalorgy/screens/User/createuser.dart';

import 'home.dart';

class AuthScreen extends StatefulWidget {
  //final Function(int, String) onChangedStep;

  const AuthScreen({Key? key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final RegExp emailRegex = RegExp(r"[a-z0-9\._-]+@[a-z0-9\._-]+\.[a-z]+");
  bool _isSecret = true;
  String _password = '';

  String _email = '';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Authentification',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),),
        backgroundColor: Colors.black,

      ),
        body: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(
              horizontal: 30.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const Text('Entrez vos identifiants'),
                      const SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        onChanged: (value) => setState(() => _email = value),
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) =>
                        value!.isEmpty || !emailRegex.hasMatch(value)
                            ? 'Entrez un email valide'
                            : null,
                        decoration: InputDecoration(
                            hintText: 'Ex: john.doe@domain.tld',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            )),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        onChanged: (value) => setState(() => _password = value),
                        validator: (value) => value!.length < 6
                            ? 'Le mot de passe doit contenir plus de 6 charac.'
                            : null,
                        obscureText: _isSecret,
                        decoration: InputDecoration(
                            suffixIcon: InkWell(
                              onTap: () =>
                                  setState(() => _isSecret = !_isSecret),
                              child: Icon(
                                !_isSecret
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                              ),
                            ),
                            hintText: 'Ex: gh!D4Yhd',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            )),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Colors.black
                        ),
                        onPressed: (!emailRegex.hasMatch(_email) || _password.length < 6)
                            ? null
                            : () {
                          verifUser();
                          },
                        child: Text(
                          'Connexion'.toUpperCase(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontFamily: 'BurnTheWitch'
                          ),
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.black
                        ),
                          onPressed: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const CreateUser(admin: false),
                                )
                            );
                            },
                          child: Text(
                            'S\'inscrire'.toUpperCase(),
                            style: const TextStyle(
                                color: Colors.white,
                                fontFamily: 'BurnTheWitch'
                            ),
                          )
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
    );
  }

  Future<void> verifUser() async {
    try {
      final response = await http.post(
          Uri.parse("${Env.URL_PREFIX}/User/verifUser.php"),
          body: {
            "Mail": _email,
            "Password" : Env.chiffrageFunction(_password, true)
          }
      );
      final items = json.decode(response.body);
      if(items == 2) {
        Navigator.pushAndRemoveUntil<void>(
          context,
          MaterialPageRoute<void>(builder: (BuildContext context) => const HomePage(admin: true)),
          ModalRoute.withName("/")
        );
      }else if (items == 1){
        Navigator.pushAndRemoveUntil<void>(
            context,
            MaterialPageRoute<void>(builder: (BuildContext context) => const HomePage(admin: false)),
            ModalRoute.withName("/")
        );
      }else {
        MyApp.displayDialog(context, "Error", "Erreur de connexion");
      }
    }on SocketException {
      MyApp.displayDialog(context, "Error", "Erreur de connexion");
    }

  }
}