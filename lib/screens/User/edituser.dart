import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:encrypt/encrypt.dart' as crypt;

import '../../../Services/api_service.dart';
import '../../../models/user.dart';
import 'detailsuser.dart';

class EditUser extends StatefulWidget {
  final User user;

  const EditUser({required this.user});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<EditUser> {
  // This is  for form validations
  final formKey = GlobalKey<FormState>();
  bool _isSecret = true;

  // This is for text onChange
  TextEditingController NomController = TextEditingController();
  TextEditingController PrenomController = TextEditingController();
  TextEditingController MailController = TextEditingController();
  TextEditingController PasswordController = TextEditingController();



  // Http post request
  Future editUser() async {
    //Récupération des champs
    //Préparation de la requête à énvoyer au serveur

    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/User/updateUser.php"),
          body: {
            "id": widget.user.id.toString(),
            "Nom": NomController.text,
            "Prenom": PrenomController.text,
            "Mail": MailController.text,
            "Password": Env.chiffrageFunction(PasswordController.text, true)
          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        NomController = TextEditingController(text: widget.user.nom);
        PrenomController = TextEditingController(text: widget.user.prenom);
        MailController = TextEditingController(text: widget.user.mail);
        PasswordController = TextEditingController(text: widget.user.password);
      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }

  }

  void _onConfirm(context) async {
    await editUser();
  }

  @override
  void initState() {
    NomController = TextEditingController(text: widget.user.nom);
    PrenomController = TextEditingController(text: widget.user.prenom);
    MailController = TextEditingController(text: widget.user.mail);
    PasswordController = TextEditingController(text: widget.user.password);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Modifier",
            style: TextStyle(
            fontFamily: 'BurnTheWitch'
        ),),
        backgroundColor: Colors.black,

      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.black
          ),
          child: const Text('CONFIRM'),
          onPressed: () {
            _onConfirm(context);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => DetailsUser(user: widget.user)));
          },
        ),
      ),
      body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields(){
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              TextField(
                controller: NomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              TextField(
                controller: PrenomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              TextField(
                controller: MailController,
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                    label: Text('e-mail'),
                    prefixIcon: Icon(Icons.email)
                ),
              ),
              TextField(
                controller: PasswordController,
                textInputAction: TextInputAction.done,
                obscureText: _isSecret,
                decoration: InputDecoration(
                    label: const Text('Mot de passe'),
                    prefixIcon: const Icon(Icons.password),
                    suffixIcon: InkWell(
                    onTap: () =>
                        setState(() => _isSecret = !_isSecret),
                    child: Icon(
                      !_isSecret
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                  ),
                  hintText: 'Ex: gh!D4Yhd',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}