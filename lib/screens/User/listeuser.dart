import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../../../Services/api_service.dart';
import '../../../models/user.dart';
import 'detailsuser.dart';
import 'createuser.dart';


class ListeUser extends StatefulWidget {
  const ListeUser({Key? key}) : super(key: key);
  @override
  _ListeUserPageState createState() => _ListeUserPageState();
}

class _ListeUserPageState extends State<ListeUser> {
  late Future<List<User>> users;
  final userListKey = GlobalKey<_ListeUserPageState>();

  @override
  void initState() {
    super.initState();
    users = getUserList();
  }

  Future<List<User>> getUserList() async {

    final response = await http.get(
        Uri.parse("${Env.URL_PREFIX}/User/listUser.php"));

    final items = json.decode(response.body).cast<Map<String, dynamic>>();

    List<User> users = items.map<User>((json) {

      return User.fromJson(json);
    }).toList();
    return users;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: userListKey,
      appBar: AppBar(
        title: const Text('Liste d\'Utilisateur',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),
        ),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: FutureBuilder<List<User>>(
          future: users,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return const CircularProgressIndicator();
            // Render user lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: const Icon(Icons.person),
                    trailing: const Icon(Icons.view_list),
                    title: Text(
                      data.nom,
                      style: const TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => DetailsUser(user: data)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (_) {
            return const CreateUser(admin: true,);
          }));
        },
      ),
    );
  }
}