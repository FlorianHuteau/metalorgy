import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../../Services/api_service.dart';
import '../../../models/user.dart';
import 'edituser.dart';

class DetailsUser extends StatefulWidget {
  final User user;
  const DetailsUser({required this.user});

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<DetailsUser> {
  void deleteUser(context) async {
    await http.post(
        Uri.parse("${Env.URL_PREFIX}/User/deleteUser.php"),
      body: {
        'id': widget.user.id.toString(),
      },
    );
    // Navigator.pop(context);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content:const Text('Are you sure you want to delete this?'),
          actions: <Widget>[
            ElevatedButton(
              child: const Icon(Icons.cancel),
              onPressed: () => Navigator.of(context).pop(),
            ),
            ElevatedButton(
              child: const Icon(Icons.check_circle),
              onPressed: () => deleteUser(context),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    String password = Env.chiffrageFunction(widget.user.password, false);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Details d\'Utilisateur',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),),
        backgroundColor: Colors.black,

        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.delete),
            onPressed: () => confirmDelete(context),
          ),
        ],
      ),
      body: Container(
        height: 270.0,
        padding: const EdgeInsets.all(35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Nom : ${widget.user.nom}",
              style:const TextStyle(fontSize: 20),
            ),
            const Padding(
              padding: EdgeInsets.all(10),
            ),
            Text(
              "Prenom : ${widget.user.prenom}",
              style:const TextStyle(fontSize: 20),
            ),
            const Padding(
              padding: EdgeInsets.all(10),
            ),
            Text(
              "Email : ${widget.user.mail}",
              style: const TextStyle(fontSize: 20),
            ),
            const Padding(
              padding: EdgeInsets.all(10),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        child: const Icon(Icons.edit),
        onPressed: () => Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => EditUser(user: widget.user),

          ),

        ),
      ),
    );
  }
}