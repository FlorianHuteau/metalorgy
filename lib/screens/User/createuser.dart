import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../Services/api_service.dart';
import '../../../main.dart';

class CreateUser extends StatefulWidget {
  final bool admin;
  const CreateUser({Key? key, required this.admin}) : super(key: key);

  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<CreateUser> {

  final formKey = GlobalKey<FormState>();
  final RegExp emailRegex = RegExp(r"[a-z0-9\._-]+@[a-z0-9\._-]+\.[a-z]+");
  bool _isSecret = true;
  String _password = '';
  bool isChecked = false;
  String _email = '';
  // Handles text onchange
  TextEditingController nomController = TextEditingController();
  TextEditingController prenomController = TextEditingController();



  // Http post request to create new data
  Future _createUser() async {
    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/User/createUser.php"),
          body: {
            "Nom": nomController.text,
            "Prenom": prenomController.text,
            "Mail": _email,
            "Password": Env.chiffrageFunction(_password, true),
            "Admin": isChecked.toString()
          }
      );
      final bool result  = json.decode(responseRegister.body);


      //final decrypted = encrypter.decrypt64(pass, iv: iv);
      if(responseRegister.statusCode == 200){
        if (!result){
          MyApp.displayDialog(context, "Error", "Email déjà utilisé");
        }else {
          SnackBar snackBarSuccess =
              const SnackBar(content: Text("Enregistrement réussie"));
          ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
          //Vider nos TextField
          nomController.clear();
          prenomController.clear();
          _password = '';
          _email = '';
        }
      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Create User",
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),),
        backgroundColor: Colors.black,

      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton(
          child: const Text("CONFIRMER",
          style: TextStyle(
            fontFamily: 'BurnTheWitch'
          ),),
          style: ElevatedButton.styleFrom(
              primary: Colors.black
          ),
          onPressed: (nomController.text == '' || prenomController.text == '' || !emailRegex.hasMatch(_email) || _password.length < 6)
                      ? null
                      : () async {
            await _createUser();
          },
        ),
      ),
      body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields() {
    return SizedBox(
      height: double.infinity,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              TextField(
                controller: nomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              TextField(
                controller: prenomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              TextFormField(
                //controller: MailController,
                keyboardType: TextInputType.emailAddress,
                onChanged: (value) =>
                    setState(() => _email = value),
                textInputAction: TextInputAction.next,
                validator: (value) =>
                value!.isEmpty || !emailRegex.hasMatch(value)
                    ? 'Please enter a valid email'
                    : null,
                decoration: const InputDecoration(
                    label: Text('e-mail'),
                    prefixIcon: Icon(Icons.email)
                ),
              ),
              TextFormField(
                //controller: PasswordController,
                textInputAction: TextInputAction.done,
                onChanged: (value) => setState(() => _password = value),
                validator: (value) =>
                value!.length < 6
                    ? 'Enter a passwords. 6 caracters min required.'
                    : null,
                obscureText: _isSecret,
                decoration: InputDecoration(
                  label: const Text('Mot de passe'),
                  prefixIcon: const Icon(Icons.password),
                  suffixIcon: InkWell(
                    onTap: () =>
                        setState(() => _isSecret = !_isSecret),
                    child: Icon(
                      !_isSecret
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                  ),
                ),
              ),
              if(widget.admin)...[
                Row(
                  children: [
                    const Icon(Icons.admin_panel_settings),
                    const Text('Administrateur'),
                    Checkbox(
                      checkColor: Colors.white,
                      value: isChecked,
                      onChanged: (bool? value) {
                        setState(() {
                          isChecked = value!;
                        });
                      },
                    )
                  ],
                )
              ]
            ],
          ),
        ),
      ),
    );
  }
}