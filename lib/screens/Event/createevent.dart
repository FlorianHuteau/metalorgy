import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:metalorgy/models/style.dart';

import '../../../Services/api_service.dart';

class CreateEvent extends StatefulWidget{

  const CreateEvent({Key? key}) : super(key: key);

  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<CreateEvent> {
  // Required for form validations
  final formKey = GlobalKey<FormState>();
  late List<Style> listStyle;
  late String nomStyle;
  // Handles text onchange
  TextEditingController nomController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateDebController = TextEditingController();
  TextEditingController dateFinController = TextEditingController();
  TextEditingController addresseController = TextEditingController();

  @override
  void initState() {
    getStyleList();
    super.initState();

  }
  Future<void> getStyleList() async {
    try{
      final response = await http.get(
          Uri.parse("${Env.URL_PREFIX}/style/liststyle.php"));
      if(response.statusCode == 200){
        final items = json.decode(response.body).cast<Map<String, dynamic>>();
        List<Style> styles = items.map<Style>((json) {
          return Style.fromJson(json);
        }).toList();
        listStyle = styles;
      } else  if (response.statusCode >0){
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + response.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  Future _createEvent() async {

    //Préparation de la requête à énvoyer au serveur
    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Event/createEvent.php"),
          body: {
            "NomEvent": nomController.text,
            "DescriptionEvent": descriptionController.text,
            "DateDebEvent": DateFormat('yyyy-MM-dd HH:mm:ss').format(DateFormat("HH:mm dd-MM-yyyy").parse(dateDebController.text)),
            "DateFinEvent": DateFormat('yyyy-MM-dd HH:mm:ss').format(DateFormat("HH:mm dd-MM-yyyy").parse(dateFinController.text)),
            "AdresseEvent":addresseController.text,
            "id_Style": listStyle.firstWhere((element) => element.nom == nomStyle).id.toString()
          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        nomController.clear();
        descriptionController.clear();
      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  void _onConfirm(context) async {
    await _createEvent();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset : true,
        appBar: AppBar(
          title: const Text("Create Event",
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
          backgroundColor: Colors.black,

        ),
        bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            child: const Text("CONFIRMER",
              style: TextStyle(
                  fontFamily: 'BurnTheWitch'
              ),),
            style: ElevatedButton.styleFrom(
                primary: Colors.black
            ),
            onPressed: () {
              _onConfirm(context);

            },
          ),
        ),
        body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields(){
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              TextField(
                controller: nomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom de l\'event'),
                    prefixIcon: Icon(Icons.event)
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:<Widget>[
                  Expanded(child: DateTimeField(
                    controller: dateDebController,
                    format: DateFormat("HH:mm dd-MM-yyyy"),
                    decoration: const InputDecoration(
                        label: Text('Date de début'),
                    ),
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                          context: context,
                          firstDate: DateTime(2000),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100));
                      if (date != null) {
                        final time = await showTimePicker(
                          context: context,
                          initialTime:
                          TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                        );
                        return DateTimeField.combine(date, time);
                      } else {
                        return currentValue;
                      }
                    },
                  ),
                  ),
                  const Text('  '),

                  Expanded(child: DateTimeField(
                    controller: dateFinController,
                    format: DateFormat("HH:mm dd-MM-yyyy"),
                    decoration: const InputDecoration(
                        label: Text('Date de fin'),
                    ),
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                          context: context,
                          firstDate: DateTime(2000),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100));
                      if (date != null) {
                        final time = await showTimePicker(
                          context: context,
                          initialTime:
                          TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                        );
                        return DateTimeField.combine(date, time);
                      } else {
                        return currentValue;
                      }
                    },
                  ),
                  )
                ],
              ),

              TextField(
                controller: descriptionController,
                textInputAction: TextInputAction.done,
                decoration: const InputDecoration(
                    label: Text('Description'),
                    prefixIcon: Icon(Icons.align_horizontal_left)
                ),
              ),
              TextField(
                controller: addresseController,
                textInputAction: TextInputAction.done,
                decoration: const InputDecoration(
                    label: Text('Addresse'),
                    prefixIcon: Icon(Icons.place)
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }


}