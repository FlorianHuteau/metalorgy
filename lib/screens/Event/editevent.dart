import 'dart:io';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../../../Services/api_service.dart';
import '../../../models/event.dart';
import 'detailsevent.dart';


class EditEvent extends StatefulWidget {
  final Event event;

  const EditEvent({Key? key, required this.event}) : super(key: key);

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<EditEvent> {
  // This is  for form validations
  final formKey = GlobalKey<FormState>();

  // This is for text onChange
  TextEditingController nomController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateDebController = TextEditingController();
  TextEditingController dateFinController = TextEditingController();
  TextEditingController addresseController = TextEditingController();

  // Http post request
  Future editEvent() async {
    //Récupération des champs
    //Préparation de la requête à énvoyer au serveur

    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Event/updateEvent.php"),
          body: {
            "id": widget.event.id.toString(),
            "NomEvent": nomController.text,
            "DescriptionEvent": descriptionController.text,
            "DateDebEvent": dateDebController.text,
            "DateFinEvent": dateFinController.text,
            "AdresseEvent": addresseController.text
          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        nomController = TextEditingController(text: widget.event.nom);
        descriptionController = TextEditingController(text: widget.event.description);
        dateDebController = TextEditingController(text: DateFormat("HH:mm dd-MM-yyyy").format(widget.event.dateDebut));
        dateFinController = TextEditingController(text: DateFormat("HH:mm dd-MM-yyyy").format(widget.event.dateFin));
        addresseController = TextEditingController(text: widget.event.adresse);

      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  void _onConfirm(context) async {
    await editEvent();
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => DetailsEvent(event: widget.event, admin: true)));
  }

  @override
  void initState() {
    nomController = TextEditingController(text: widget.event.nom);
    descriptionController = TextEditingController(text: widget.event.description);
    dateDebController = TextEditingController(text: DateFormat("HH:mm dd-MM-yyyy").format(widget.event.dateDebut));
    dateFinController = TextEditingController(text: DateFormat("HH:mm dd-MM-yyyy").format(widget.event.dateFin));
    addresseController = TextEditingController(text: widget.event.adresse);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Edit Event",
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
          backgroundColor: Colors.black,

        ),
        bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            child: const Text('CONFIRMER',
              style: TextStyle(
                  fontFamily: 'BurnTheWitch'
              ),),
            style: ElevatedButton.styleFrom(
                primary: Colors.black
            ),
            onPressed: () {
              _onConfirm(context);

            },
          ),
        ),
        body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields(){
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              TextField(
                controller: nomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:<Widget>[
                  Expanded(child: DateTimeField(
                    controller: dateDebController,
                    format: DateFormat("HH:mm dd-MM-yyyy"),
                    decoration: const InputDecoration(
                      label: Text('Date de début'),
                    ),
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                          context: context,
                          firstDate: DateTime(2000),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100));
                      if (date != null) {
                        final time = await showTimePicker(
                          context: context,
                          initialTime:
                          TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                        );
                        return DateTimeField.combine(date, time);
                      } else {
                        return currentValue;
                      }
                    },
                  ),
                  ),
                  const Text('  '),

                  Expanded(child: DateTimeField(
                    controller: dateFinController,
                    format: DateFormat("HH:mm dd-MM-yyyy"),
                    decoration: const InputDecoration(
                      label: Text('Date de fin'),
                    ),
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                          context: context,
                          firstDate: DateTime(2000),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100));
                      if (date != null) {
                        final time = await showTimePicker(
                          context: context,
                          initialTime:
                          TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                        );
                        return DateTimeField.combine(date, time);
                      } else {
                        return currentValue;
                      }
                    },
                  ),
                  )
                ],
              ),

              TextField(
                controller: descriptionController,
                textInputAction: TextInputAction.done,
                decoration: const InputDecoration(
                    label: Text('Description'),
                    prefixIcon: Icon(Icons.align_horizontal_left)
                ),
              ),
              TextField(
                controller: addresseController,
                textInputAction: TextInputAction.done,
                decoration: const InputDecoration(
                    label: Text('Addresse'),
                    prefixIcon: Icon(Icons.place)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}