import 'dart:convert';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metalorgy/models/scene.dart';
import '../../models/style.dart';
import '../Programmation/listeprogbyscene.dart';
import '../Scene/createscene.dart';

import '../../../Services/api_service.dart';
import '../../../models/event.dart';

import '../Style/detailsstyle.dart';
import 'editevent.dart';

class DetailsEvent extends StatefulWidget {
  final Event event;
  final bool admin;
  const DetailsEvent({Key? key, required this.event, required this.admin}) : super(key: key);

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<DetailsEvent> {
  late Future<List<Scene>> scenes;
  late Future<List<Style>> styles;



  @override
  void initState() {
    scenes = getEventScenes();
    styles = getStyleListEvent();
    super.initState();
  }

  Future<List<Style>> getStyleListEvent() async {
    final response = await http.post(
        Uri.parse("${Env.URL_PREFIX}/Event/listeStyleEvent.php"),
        body: {
          "idEvent": widget.event.id.toString()
        }
    );
    final items = json.decode(response.body).cast<Map<String, dynamic>>();
    List<Style> styles = items.map<Style>((json) {
      return Style.fromJson(json);
    }).toList();
    return styles;
  }
  void deleteEvent(context) async {
    await http.post(
      Uri.parse("${Env.URL_PREFIX}/Event/deleteEvent.php"),
      body: {
        'id': widget.event.id.toString(),
      },
    );
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: const Text('Are you sure you want to delete this?'),
          actions: <Widget>[
            ElevatedButton(
              child: const Icon(Icons.cancel),
              onPressed: () => Navigator.of(context).pop(),
            ),
            ElevatedButton(
              child: const Icon(Icons.check_circle),
              onPressed: () => deleteEvent(context),
            ),
          ],
        );
      },
    );
  }


  Future<List<Scene>> getEventScenes() async {
    final response = await http.post(
        Uri.parse("${Env.URL_PREFIX}/Event/getEventScenes.php"),
        body: {
          "id": widget.event.id.toString()
        }
    );

    final items = json.decode(response.body).cast<Map<String, dynamic>>();
    List<Scene> scenes = items.map<Scene>((json) {
      return Scene.fromJson(json);
    }).toList();
    return scenes;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Détails de l\'Événement',
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
          backgroundColor: Colors.black,

          actions: <Widget>[
            if(widget.admin)...[
              IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () => confirmDelete(context),
              ),
            ]
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(15),
          child: ListView(
            children: <Widget>[
              Text(
                "Nom : ${widget.event.nom}",
                style: const TextStyle(fontSize: 20),
              ),
              const Padding(
                padding: EdgeInsets.all(10),
              ),
              Text(
                "Description : ${widget.event.description}",
                style: const TextStyle(fontSize: 20),
              ),
              const Padding(
                padding: EdgeInsets.all(10),
              ),
              Text(
                "Dû ${widget.event.dateDebut} au ${widget.event.dateFin}",
                style: const TextStyle(fontSize: 20),
              ),
              const Padding(
                padding: EdgeInsets.all(10),
              ),
              Text(
                "Adresse : ${widget.event.adresse}",
                style: const TextStyle(fontSize: 20),
              ),
              const Padding(
                padding: EdgeInsets.all(10),
              ),
              const Text(
                "Programmation par scènes :",
                style: TextStyle(fontSize: 20),
              ),
              FutureBuilder<List<Scene>>(
                future: scenes,
                builder: (BuildContext context, AsyncSnapshot snaphot) {
                  if (!snaphot.hasData) {
                    return const CircularProgressIndicator();
                  }
                  return ListView.builder(
                      itemCount: snaphot.data.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        var data = snaphot.data[index];
                        return Card(
                          child: ListTile(
                            leading: const Icon(Icons.album),
                            title: Text(
                                data.nom
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>
                                    ListeProgrammationByScene(
                                        event: widget.event,
                                        scene: data,
                                        admin: widget.admin)),
                              );
                            },
                          ),
                        );
                      }
                  );
                },
              ),
              const Text(
                "Style de musique :",
                style: TextStyle(fontSize: 20),
              ),
              FutureBuilder<List<Style>>(
                  future: styles,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (!snapshot.hasData) {
                      return const CircularProgressIndicator();
                    }
                    return ListView.builder(
                        itemCount: snapshot.data.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          var data = snapshot.data[index];
                          return Card(
                              child: ListTile(
                                leading: const Icon(Icons.album),
                                title: Text(
                                    data.nom
                                ),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) =>
                                          DetailsStyle(
                                              style: data, admin: widget.admin))
                                  );
                                },
                              )
                          );
                        }
                    );
                  }
              )
            ],
          ),
        ),
        floatingActionButton: Visibility(
            visible: widget.admin,
            child: SpeedDial(
                backgroundColor: Colors.black,
                children: [
                  SpeedDialChild(
                      child: const Icon(Icons.access_time),
                      onTap: () =>
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      CreateScene(idEvent: widget.event.id)))
                  ),
                  SpeedDialChild(
                    child: const Icon(Icons.edit),
                    onTap: () =>
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                EditEvent(event: widget.event),
                          ),
                        ),
                  )
                ]
            )
        )
    );
  }
}
