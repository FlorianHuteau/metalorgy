import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metalorgy/models/event.dart';
import 'dart:convert';

import '../../../Services/api_service.dart';
import '../../../models/event.dart';
import 'detailsevent.dart';
import 'createevent.dart';

class ListeEvent extends StatefulWidget{
  final bool admin;
  const ListeEvent({Key? key, required this.admin}) : super(key: key);
  @override
  _ListeEventPageState createState() => _ListeEventPageState();
}

class _ListeEventPageState extends State<ListeEvent>{
  late Future<List<Event>> events;
  final eventListKey = GlobalKey<_ListeEventPageState>();


  @override
  void initState() {
    super.initState();
    events = getEventList();
  }

  Future<List<Event>> getEventList() async {
    final response = await http.get(
        Uri.parse("${Env.URL_PREFIX}/Event/listEvent.php"));

    final items = json.decode(response.body).cast<Map<String, dynamic>>();
    List<Event> events = items.map<Event>((json) {
      return Event.fromJson(json);
    }).toList();
    return events;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: eventListKey,
      appBar: AppBar(
        title: const Text('Liste d\'Événement',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),),
        backgroundColor: Colors.black,

      ),
      body: Center(
        child: FutureBuilder<List<Event>>(
          future: events,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return const CircularProgressIndicator();
            // Render Event lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: const Icon(Icons.event),
                    trailing: const Icon(Icons.view_list),
                    title: Text(
                      data.nom,
                      style: const TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DetailsEvent(event: data, admin: widget.admin)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: Visibility(
        visible: widget.admin,
        child: FloatingActionButton(
          child: const Icon(Icons.add),
          backgroundColor: Colors.black,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return const CreateEvent();
            }));
          },
        )
      ),
    );
  }
}