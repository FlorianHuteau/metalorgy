import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:metalorgy/models/programmation.dart';
import 'dart:convert';

import '../Artiste/detailsartiste.dart';
import '../../../Services/api_service.dart';
import '../../../models/programmation.dart';
import '../../../models/event.dart';
import '../../../models/artiste.dart';
import '../../../models/scene.dart';
import 'createprog.dart';

class ListeProgrammationByScene extends StatefulWidget{
  final Event event;
  final Scene scene;
  final bool admin;
  const ListeProgrammationByScene({Key? key, required this.event, required this.scene, required this.admin}) : super(key: key);

  @override
  _ListeProgrammationByScenePageState createState() => _ListeProgrammationByScenePageState();
}

class _ListeProgrammationByScenePageState extends State<ListeProgrammationByScene>{
  late Future<List<Programmation>> programmations;
  late List<Artiste> listArtiste;

  final programmationListKey = GlobalKey<_ListeProgrammationByScenePageState>();


  @override
  void initState() {
    super.initState();
    programmations = getProgrammationList();
  }

  Future<List<Programmation>> getProgrammationList() async {
    try {
      bool returnArtiste = await getEventArtistes();
      final response = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Programmation/listProgByScene.php"),
          body: {
            "id_Event": widget.event.id.toString(),
            "id_Scene": widget.scene.id.toString(),
          }
      );
      if(response.statusCode == 200 && returnArtiste) {
        final items = json.decode(response.body).cast<Map<String, dynamic>>();
        List<Programmation> programmations = items.map<Programmation>((json) {
          int idArtiste = json['id_Artiste'];
          int index = listArtiste.indexWhere((element) => element.id == idArtiste);
          Artiste artiste = listArtiste[index];
          return Programmation.fromJson(json, widget.event, artiste, widget.scene);
        }).toList();
        return programmations;
      }else  if (response.statusCode >0 || !returnArtiste){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + response.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    }on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);

    }
    return programmations;

  }

  Future<bool> getEventArtistes() async {
    try{
      final response = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Artiste/getArtistesByEvent.php"),
          body: {
            "idEvent": widget.event.id.toString(),
            "DateDebutEvent": DateFormat('yyyy-MM-dd').format(widget.event.dateDebut),
            "DateFinEvent": DateFormat('yyyy-MM-dd').format(widget.event.dateFin)
          }
      );
      if(response.statusCode == 200) {
        final items = json.decode(response.body).cast<Map<String, dynamic>>();
        List<Artiste> artistes = items.map<Artiste>((json){
          return Artiste.fromJson(json);
        }).toList();
         listArtiste = artistes;
         return true;
      }else  if (response.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + response.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: programmationListKey,
      appBar: AppBar(
        title: const Text('Programmation',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),),
        backgroundColor: Colors.black,

      ),
      body: Center(
        child: FutureBuilder<List<Programmation>>(
          future: programmations,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return const CircularProgressIndicator();
            // Render Programmation lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    //leading: const Icon(Icons.programmation),
                    trailing: const Icon(Icons.view_list),
                    title: Text(
                      data.artiste.nom,
                      style: const TextStyle(fontSize: 20),
                    ),
                    subtitle: Text(data.date + ' à ' + data.heure),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DetailsArtiste(artiste: data.artiste, admin: widget.admin)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: Visibility(
        visible: widget.admin,
        child: FloatingActionButton(
          child: const Icon(Icons.add),
          backgroundColor: Colors.black,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return CreateProgrammation(event: widget.event, scene: widget.scene);
            }));
          },
        ),
      )

    );
  }
}