import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../../../Services/api_service.dart';
import '../../../models/event.dart';
import '../../../models/scene.dart';
import '../../../models/artiste.dart';
import 'listeprogbyscene.dart';


class CreateProgrammation extends StatefulWidget{
  final Event event;
  final Scene scene;
  const CreateProgrammation({Key? key, required this.event, required this.scene}) : super(key: key);


  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<CreateProgrammation> {
  // Required for form validations
  final formKey = GlobalKey<FormState>();
  late List<String> iterable;
  late List<Artiste> listArtistes;
  late String nomArtiste;
  List<String> listDateEvent = [];
  late String dateEvent;

  // Handles text onchange
  TextEditingController heureController = TextEditingController();

  @override
  void initState() {
    getArtisteList();
    listDate();
    super.initState();

  }
  void listDate(){
    int jourDebut = int.parse(DateFormat('dd').format(widget.event.dateDebut));
    int jourFin = int.parse(DateFormat('dd').format(widget.event.dateFin));
    String moisDebut = DateFormat('MM').format(widget.event.dateDebut);
    String moisFin = DateFormat('MM').format(widget.event.dateFin);

    if(jourDebut < jourFin) {
      for (int i = jourDebut; i <= jourFin; i++) {
          listDateEvent.add(i.toString() + '/' + moisDebut);
      }
    }else{
      if(['1', '3', '5', '7', '8', '10', '12'].contains(moisDebut)){
        int i =jourDebut;
        listDateEvent.add(jourDebut.toString() + '/' + moisDebut);
        while(i != jourFin){
          i = i==31 ? 1 : i + 1;
          i>jourDebut ? listDateEvent.add(i.toString() + '/' + moisDebut) : listDateEvent.add(i.toString() + '/' + moisFin);
        }

      }else if(['4','6','9','11'].contains(moisDebut)){
        int i =jourDebut;
        listDateEvent.add(jourDebut.toString() + '/' + moisDebut);
        while(i != jourFin){
          i = i==30 ? 1 : i + 1;
          i>jourDebut ? listDateEvent.add(i.toString() + '/' + moisDebut) : listDateEvent.add(i.toString() + '/' + moisFin);
        }

      }else if(moisDebut == '2'){
        int i =jourDebut;
        listDateEvent.add(jourDebut.toString() + '/' + moisDebut);
        while(i != jourFin){
          i = i==28 ? 1 : i + 1;
          i>jourDebut ? listDateEvent.add(i.toString() + '/' + moisDebut) : listDateEvent.add(i.toString() + '/' + moisFin);
        }

      }
    }
    dateEvent = listDateEvent[0];
  }

  void getArtisteList() async {
    try{

      final response = await http.get(
        Uri.parse("${Env.URL_PREFIX}/Artiste/listArtiste.php"));
      if(response.statusCode == 200){
        final items = json.decode(response.body).cast<Map<String, dynamic>>();
        List<Artiste> artistes = items.map<Artiste>((json) {
          return Artiste.fromJson(json);
        }).toList();
        listArtistes = artistes;
      } else  if (response.statusCode >0){
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + response.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }
  // Http post request to create new data
  Future _createProgrammation() async {
    try{
      String anEvent = DateFormat('yyyy').format(widget.event.dateDebut);
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Programmation/createProg.php"),
          body: {
            "id_Event": widget.event.id.toString(),
            "id_Artiste": listArtistes.firstWhere((element) => element.nom == nomArtiste).id.toString(),
            "id_Scene":widget.scene.id.toString(),
            "HeureProg":heureController.text,
            "DateProg": DateFormat('yyyy-MM-dd').format(DateFormat("dd/MM/yyyy").parse(dateEvent + '/' + anEvent))
          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        heureController.clear();
      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  void _onConfirm(context) async {
    await _createProgrammation();
    Navigator.pop(context);
    Navigator.pop(context);
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ListeProgrammationByScene(event: widget.event, scene: widget.scene, admin: true,))
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Ajouter une programmation",
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),
          ),
          backgroundColor: Colors.black,

        ),
        bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            child: const Text("CONFIRMER",
            style: TextStyle(
              fontFamily: 'BurntheWitch'
            ),),
            style: ElevatedButton.styleFrom(
              primary: Colors.black
          ),
            onPressed: () {
              _onConfirm(context);

            },
          ),
        ),
        body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields() {
    return Container(
        height: double.infinity,
        padding: const EdgeInsets.all(20),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
                children: [
                  const Text("Artiste"),
                  Expanded(child: Autocomplete<String>(
                    optionsBuilder: (TextEditingValue textEditingValue) {
                      if (textEditingValue.text == '') {
                        return const Iterable<String>.empty();
                      }
                      try {
                        late List<String> listNom = [];
                        listArtistes.where((artiste) =>
                            artiste.nom.contains(textEditingValue.text))
                            .forEach((element) {
                          listNom.add(element.nom);
                        });
                        return listNom;
                      } on SocketException {
                        //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
                        SnackBar snackBarFailure = const SnackBar(content: Text(
                            "Nous avons des difficultés à joindre le serveur"));
                        ScaffoldMessenger.of(context).showSnackBar(
                            snackBarFailure);
                        return const Iterable<String>.empty();
                      }
                    },
                    onSelected: (String selection) {
                      nomArtiste = selection;
                    },
                  ),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    //scrollDirection: Axis.horizontal,
                    itemCount: listDateEvent.length,
                    itemBuilder: (BuildContext context, int index) {
                      var data = listDateEvent[index];
                      return Card(
                        child: RadioListTile<String>(
                          title: Text(data),
                          value: data,
                          groupValue: dateEvent,
                          onChanged: (String? value) {
                            setState(() {
                              dateEvent = value!;
                            });
                          },
                        ),

                      );
                    },
                  ),
                  Expanded(child: DateTimeField(
                    controller: heureController,
                    format: DateFormat("HH:mm"),
                    onShowPicker: (context, currentValue) async {
                      final time = await showTimePicker(
                        context: context,
                        initialTime: TimeOfDay.fromDateTime(
                            currentValue ?? DateTime.now()),
                        builder: (context, child) =>
                            MediaQuery(data: MediaQuery.of(context).copyWith(
                                alwaysUse24HourFormat: true), child: child!),
                      );
                      return DateTimeField.convert(time);
                    },
                  ),
                  ),
                ]
            ),
          ),
        )
    );
  }
}