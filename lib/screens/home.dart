import 'package:flutter/material.dart';

import 'Artiste/listeartiste.dart';
import 'Event/listeevent.dart';
import 'Style/listestyle.dart';
import 'User/listeuser.dart';
import 'auth.dart';


class HomePage extends StatelessWidget{
  final bool admin;
  const HomePage({Key? key, required this.admin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Accueil',
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),
          ),
          backgroundColor: Colors.black,
          actions: <Widget>[
            IconButton(onPressed: (){
              Navigator.of(context).pushNamedAndRemoveUntil("/", (route) => false);},
                icon : const Icon(Icons.logout)
            )
          ]
        ),
        body: Center(
            child: ListView(
              children: <Widget>[
                if(admin)...[
                Card(
                    child: ListTile(
                      title: const Text('Utilisateurs',
                        style: TextStyle(
                            fontFamily: 'BurnTheWitch'
                        ),),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ListeUser()),
                        );
                      },
                    )
                ),
                ],
                Card(
                    child: ListTile(
                      title: const Text('Artistes',
                        style: TextStyle(
                            fontFamily: 'BurnTheWitch'
                        ),),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListeArtiste(admin : admin)),
                        );
                      },
                    )
                ),
                Card(
                    child: ListTile(
                      title: const Text('Événements',
                        style: TextStyle(
                            fontFamily: 'BurnTheWitch'
                        ),),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListeEvent(admin : admin)),
                        );
                      },
                    )
                ),
                Card(
                    child: ListTile(
                      title: const Text('Style de musique',
                        style: TextStyle(
                            fontFamily: 'BurnTheWitch'
                        ),),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListeStyle(admin: admin)),
                        );
                      },
                    )
                ),
                if(!admin)...[
                Card(
                    child: ListTile(
                      title: const Text('Authentification',
                        style: TextStyle(
                            fontFamily: 'BurnTheWitch'
                        ),),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const AuthScreen()),
                        );
                      },
                    )
                )]
              ],
            )
        )
    );
  }
}