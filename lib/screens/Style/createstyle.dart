import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'listestyle.dart';

import '../../../Services/api_service.dart';

class CreateStyle extends StatefulWidget{

  const CreateStyle({Function? refreshStyleList});

  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<CreateStyle> {
  // Required for form validations
  final formKey = GlobalKey<FormState>();

  // Handles text onchange
  TextEditingController NomController = TextEditingController();
  TextEditingController DescriptionController = TextEditingController();



  // Http post request to create new data
  Future _createStyle() async {
    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Style/createStyle.php"),
          body: {
            "NomStyle": NomController.text,
            "DescriptionStyle": DescriptionController.text,
          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        NomController.clear();
        DescriptionController.clear();
      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  void _onConfirm(context) async {
    await _createStyle();
    Navigator.pop(context);
    Navigator.pop(context);
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const ListeStyle(admin: true,))
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Create Style",
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
          backgroundColor: Colors.black,

        ),
        bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            child: const Text("CONFIRMER",
            style: TextStyle(
              fontFamily: 'BurnTheWitch'
            ),),
            style: ElevatedButton.styleFrom(
                primary: Colors.black
            ),
            onPressed: () {
              _onConfirm(context);

            },
          ),
        ),
        body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields(){
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              TextField(
                controller: NomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),

              TextField(
                controller: DescriptionController,
                textInputAction: TextInputAction.done,
                decoration: const InputDecoration(
                    label: Text('Description'),
                    prefixIcon: Icon(Icons.align_horizontal_left)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}