import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../../Services/api_service.dart';
import '../../../models/style.dart';
import 'editstyle.dart';

class DetailsStyle extends StatefulWidget {
  final Style style;
  final bool admin;
  const DetailsStyle({Key? key, required this.style, required this.admin}) : super(key: key);

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<DetailsStyle> {
  void deleteStyle(context) async {
    await http.post(
      Uri.parse("${Env.URL_PREFIX}/Style/deleteStyle.php"),
      body: {
        'id': widget.style.id.toString(),
      },
    );
    // Navigator.pop(context);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content:const Text('Are you sure you want to delete this?'),
          actions: <Widget>[
            ElevatedButton(
              child: const Icon(Icons.cancel),
              onPressed: () => Navigator.of(context).pop(),
            ),
            ElevatedButton(
              child: const Icon(Icons.check_circle),
              onPressed: () => deleteStyle(context),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Details Style',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),),
        backgroundColor: Colors.black,

        actions: <Widget>[
          if(widget.admin)...[
            IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () => confirmDelete(context),
            ),
          ]
        ],
      ),
      body: Container(
        height: double.infinity,
        padding: const EdgeInsets.all(35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Nom du Style : ${widget.style.nom}",
              style:const TextStyle(fontSize: 20),
            ),
            const Padding(
              padding: EdgeInsets.all(10),
            ),
            Text(
              "Description : ${widget.style.description}",
              style:const TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
      floatingActionButton: Visibility(
        visible: widget.admin,
        child: FloatingActionButton(
            child: const Icon(Icons.edit),
            backgroundColor: Colors.black,
            onPressed: () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => EditStyle(style: widget.style),
              ),
            )
        ),
      ),
    );
  }
}