import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metalorgy/models/style.dart';
import 'dart:convert';

import '../../../Services/api_service.dart';
import '../../../models/style.dart';
import 'detailsstyle.dart';
import 'createstyle.dart';

class ListeStyle extends StatefulWidget{
  final bool admin;
  const ListeStyle({Key? key, required this.admin}) : super(key: key);
  @override
  _ListeStylePageState createState() => _ListeStylePageState();
}

class _ListeStylePageState extends State<ListeStyle>{
  late Future<List<Style>> styles;
  final styleListKey = GlobalKey<_ListeStylePageState>();


  @override
  void initState() {
    super.initState();
    styles = getStyleList();
  }

  Future<List<Style>> getStyleList() async {
    final response = await http.get(
        Uri.parse("${Env.URL_PREFIX}/Style/listStyle.php"));

    final items = json.decode(response.body).cast<Map<String, dynamic>>();
    List<Style> styles = items.map<Style>((json) {
      return Style.fromJson(json);
    }).toList();
    return styles;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: styleListKey,
      appBar: AppBar(
        title: const Text('Styles de Musique',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),),
        backgroundColor: Colors.black,

      ),
      body: Center(
        child: FutureBuilder<List<Style>>(
          future: styles,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return const CircularProgressIndicator();
            // Render Style lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: const Icon(Icons.person),
                    trailing: const Icon(Icons.view_list),
                    title: Text(
                      data.nom,
                      style: const TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DetailsStyle(style: data,admin: widget.admin,)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: Visibility(
        visible: widget.admin,
        child: FloatingActionButton(
          child: const Icon(Icons.add),
          backgroundColor: Colors.black,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return const CreateStyle();
            }));
          },
        ),
      )

    );
  }
}