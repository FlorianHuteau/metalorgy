import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metalorgy/models/artiste.dart';
import 'dart:convert';

import '../../../Services/api_service.dart';
import '../../../models/artiste.dart';
import 'createartiste.dart';
import 'detailsartiste.dart';

class ListeArtiste extends StatefulWidget{
  final bool admin;

  const ListeArtiste({Key? key, required this.admin}) : super(key: key);
  @override
  _ListeArtistePageState createState() => _ListeArtistePageState();
}

class _ListeArtistePageState extends State<ListeArtiste>{
  late Future<List<Artiste>> artistes;
  final artisteListKey = GlobalKey<_ListeArtistePageState>();

  @override
  void initState() {
    super.initState();
    artistes = getArtisteList();
  }

  Future<List<Artiste>> getArtisteList() async {
    final response = await http.get(
        Uri.parse("${Env.URL_PREFIX}/Artiste/listArtiste.php"));

    final items = json.decode(response.body).cast<Map<String, dynamic>>();
    List<Artiste> artistes = items.map<Artiste>((json) {
      return Artiste.fromJson(json);
    }).toList();
    return artistes;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: artisteListKey,
      appBar: AppBar(
        title: const Text('Liste des Artistes',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),),
        backgroundColor: Colors.black,

      ),
      body: Center(
        child: FutureBuilder<List<Artiste>>(
          future: artistes,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return const CircularProgressIndicator();
            // Render Artiste lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: const Icon(Icons.person),
                    trailing: const Icon(Icons.view_list),
                    title: Text(
                      data.nom,
                      style: const TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DetailsArtiste(artiste: data, admin: widget.admin)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton:Visibility(
        visible: widget.admin,
        child: FloatingActionButton(
          child: const Icon(Icons.add),
          backgroundColor: Colors.black,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return const CreateArtiste();
            }));
          },
        ),
      )
    );
  }
}