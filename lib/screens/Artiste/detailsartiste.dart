import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metalorgy/screens/Artiste/listeartiste.dart';
import 'package:metalorgy/screens/Style/detailsstyle.dart';

import '../../../Services/api_service.dart';
import '../../../models/artiste.dart';
import '../../models/style.dart';
import 'editartiste.dart';

class DetailsArtiste extends StatefulWidget {
  final Artiste artiste;
  final bool admin;
  const DetailsArtiste({Key? key, required this.artiste, required this.admin}) : super(key: key);

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<DetailsArtiste> {
  late Future<List<Style>> styles;

  @override
  void initState(){
    styles = getStyleListArtist();

    super.initState();
  }


  Future<List<Style>> getStyleListArtist() async {
    final response = await http.post(
        Uri.parse("${Env.URL_PREFIX}/Artiste/listeStyleArtiste.php"),
        body: {
          "idArtiste": widget.artiste.id.toString()
        }
    );
    final items = json.decode(response.body).cast<Map<String, dynamic>>();
    List<Style> styles = items.map<Style>((json) {
      return Style.fromJson(json);
    }).toList();
    return styles;
  }

  void deleteArtiste(context) async {
    await http.post(
      Uri.parse("${Env.URL_PREFIX}/Artiste/deleteArtiste.php"),
      body: {
        'id': widget.artiste.id.toString(),
      },
    );
    // Navigator.pop(context);
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => const ListeArtiste(admin: true),
    ));

  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content:const Text('Êtes-vous sûr de vouloir supprimer cette artiste ?'),
          actions: <Widget>[
            ElevatedButton(
              child: const Icon(Icons.cancel),
              onPressed: () => Navigator.of(context).pop(),
            ),
              ElevatedButton(
              child: const Icon(Icons.check_circle),
              onPressed: () => deleteArtiste(context),
              ),

          ],
        );
      },
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails de l\'Artiste',
          style: TextStyle(
              fontFamily: 'BurnTheWitch'
          ),
        ),
        backgroundColor: Colors.black,

        actions: <Widget>[
          if(widget.admin)...[
            IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () => confirmDelete(context),
            ),
          ]
        ],
      ),
      body: Container(
        height: 500.0,
        padding: const EdgeInsets.all(35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Nom : ${widget.artiste.nom}",
              style:const TextStyle(fontSize: 20),
            ),
            const Padding(
              padding: EdgeInsets.all(10),
            ),
            Text(
              "Description : ${widget.artiste.description}",
              style:const TextStyle(fontSize: 20),
            ),
            const Padding(padding: EdgeInsets.all(10)),
            const Text(
              "Style de musique : ",
              style: TextStyle(fontSize: 20),
            ),
            FutureBuilder<List<Style>>(
                future: styles,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (!snapshot.hasData) {
                      return const CircularProgressIndicator();
                    }
                    return ListView.builder(
                        itemCount: snapshot.data.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          var data = snapshot.data[index];
                          return Card(
                              child: ListTile(
                                leading: const Icon(Icons.album),
                                title: Text(
                                    data.nom
                                ),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) =>
                                          DetailsStyle(
                                              style: data, admin: widget.admin))
                                  );
                                },
                              )
                          );
                        }
                    );
                  }
            )

          ],
        ),
      ),
      floatingActionButton: Visibility(
        visible: widget.admin,
        child: FloatingActionButton(
          child: const Icon(Icons.edit),
          backgroundColor: Colors.black,
          onPressed: () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => EditArtiste(artiste: widget.artiste),
              ),
            ),
        )
      ),
    );
  }
}