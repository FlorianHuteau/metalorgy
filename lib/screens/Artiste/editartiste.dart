import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metalorgy/screens/Artiste/detailsartiste.dart';

import '../../../Services/api_service.dart';
import '../../../models/artiste.dart';
import '../../models/style.dart';
import '../Style/detailsstyle.dart';


class EditArtiste extends StatefulWidget {
  final Artiste artiste;

  const EditArtiste({Key? key, required this.artiste}) : super(key: key);

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<EditArtiste> {
  // This is  for form validations
  final formKey = GlobalKey<FormState>();
  late List<Style> listStyle;
  late String nomStyle;
  late Future<List<Style>> styles;

  // This is for text onChange
  TextEditingController NomController = TextEditingController();
  TextEditingController DescriptionController = TextEditingController();

  Future<void> getStyleList() async {
    try{
      final response = await http.get(
          Uri.parse("${Env.URL_PREFIX}/style/liststyle.php"));
      if(response.statusCode == 200){
        final items = json.decode(response.body).cast<Map<String, dynamic>>();
        List<Style> styles = items.map<Style>((json) {
          return Style.fromJson(json);
        }).toList();
        listStyle = styles;
      } else  if (response.statusCode >0){
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + response.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  // Http post request
  Future editArtiste() async {
    //Récupération des champs
    //Préparation de la requête à énvoyer au serveur

    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Artiste/updateArtiste.php"),
          body: {
            "id": widget.artiste.id.toString(),
            "Nom": NomController.text,
            "Description": DescriptionController.text,
            "id_Style":listStyle.firstWhere((element) => element.nom == nomStyle).id.toString()

          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        NomController = TextEditingController(text: widget.artiste.nom);
        DescriptionController = TextEditingController(text: widget.artiste.description);

      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  void deleteStyleArtiste(context, data) async{
    await http.post(
      Uri.parse("${Env.URL_PREFIX}/Artiste/deleteStyleArtiste.php"),
      body:{
        'idArtiste' : widget.artiste.id.toString(),
        'idStyle' : data.id.toString()
      },
    );
    Navigator.of(context).pop();
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => super.widget));
  }

  void confirmDeleteStyleArtiste(context, data){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return  AlertDialog(
            content: const Text('Êtes-vous sûr de vouloir supprimer ce style de l\'artiste ?'),
            actions: <Widget>[
              ElevatedButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Icon(Icons.cancel)
              ),
              ElevatedButton(
                  onPressed: () => deleteStyleArtiste(context, data),
                  child: const Icon(Icons.check_circle)
              )
            ],
          );
        }
    );
  }
  Future<List<Style>> getStyleListArtist() async {
    final response = await http.post(
        Uri.parse("${Env.URL_PREFIX}/Artiste/listeStyleArtiste.php"),
        body: {
          "idArtiste": widget.artiste.id.toString()
        }
    );
    final items = json.decode(response.body).cast<Map<String, dynamic>>();
    List<Style> styles = items.map<Style>((json) {
      return Style.fromJson(json);
    }).toList();
    return styles;
  }

  void _onConfirm(context) async {
    await editArtiste();
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => super.widget));
  }

  @override
  void initState() {
    NomController = TextEditingController(text: widget.artiste.nom);
    DescriptionController = TextEditingController(text: widget.artiste.description);
    getStyleList();
    styles = getStyleListArtist();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Modifier l'Artiste",
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
          leading: IconButton(icon: const Icon(Icons.arrow_back),
            onPressed: () => Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => DetailsArtiste(artiste: widget.artiste, admin: true),
                ),
            ),
          ),
          backgroundColor: Colors.black,
        ),
        bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.black
            ),
            child: const Text('CONFIRMER',
            style: TextStyle(
              fontFamily: 'BurnTheWitch'
            ),),
            onPressed: () {
              _onConfirm(context);
            },
          ),
        ),
        body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields(){
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: ListView(
            children: [
              TextField(
                controller: NomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              TextField(
                controller: DescriptionController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Description'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              const Text("Style de musique"),
              FutureBuilder<List<Style>>(
                  future: styles,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (!snapshot.hasData) {
                      return const CircularProgressIndicator();
                    }
                    return ListView.builder(
                        itemCount: snapshot.data.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          var data = snapshot.data[index];
                          return Card(
                              child: ListTile(
                                leading: const Icon(Icons.album),
                                title: Text(
                                    data.nom
                                ),
                                trailing: IconButton(
                                    icon: const Icon(Icons.delete),
                                    onPressed: () => confirmDeleteStyleArtiste(context, data)
                                ),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) =>
                                          DetailsStyle(
                                              style: data, admin: true))
                                  );
                                },
                              )
                          );
                        }
                    );
                  }
              ),
              Autocomplete<String>(
                optionsBuilder: (TextEditingValue textEditingValue) {
                  if (textEditingValue.text == '') {
                    return const Iterable<String>.empty();
                  }
                  try {
                    late List<String> listNom = [];
                    listStyle.where((style) =>
                        style.nom.contains(textEditingValue.text))
                        .forEach((element) {
                      listNom.add(element.nom);
                    });
                    return listNom;
                  } on SocketException {
                    //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
                    SnackBar snackBarFailure = const SnackBar(content: Text(
                        "Nous avons des difficultés à joindre le serveur"));
                    ScaffoldMessenger.of(context).showSnackBar(
                        snackBarFailure);
                    return const Iterable<String>.empty();
                  }
                },
                onSelected: (String selection) {
                  nomStyle = selection;
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}