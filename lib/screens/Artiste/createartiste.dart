import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metalorgy/models/style.dart';

import '../../../Services/api_service.dart';

class CreateArtiste extends StatefulWidget{

  const CreateArtiste({Key? key, Function? refreshArtisteList}): super(key: key);

  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<CreateArtiste> {
  // Required for form validations
  final formKey = GlobalKey<FormState>();
  late List<Style> listStyle;
  late String nomStyle;
  List<Style> styleArtiste = [];
  // Handles text onchange
  TextEditingController nomController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  @override
  void initState() {
    getStyleList();
    super.initState();
  }

  void addStyle(){

  }
  Future<void> getStyleList() async {
    try{
      final response = await http.get(
          Uri.parse("${Env.URL_PREFIX}/style/liststyle.php"));
      if(response.statusCode == 200){
        final items = json.decode(response.body).cast<Map<String, dynamic>>();
        List<Style> styles = items.map<Style>((json) {
          return Style.fromJson(json);
        }).toList();
        listStyle = styles;
      } else  if (response.statusCode >0){
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + response.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  // Http post request to create new data
  Future _createArtiste() async {
    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Artiste/createArtiste.php"),
          body: {
            "Nom": nomController.text,
            "Description": descriptionController.text,
            "id_Style":listStyle.firstWhere((element) => element.nom == nomStyle).id.toString()
          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        nomController.clear();
        descriptionController.clear();
      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  void _onConfirm(context) async {
    await _createArtiste();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/Artiste', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Create",
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
          backgroundColor: Colors.black,

        ),
        bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            child: const Text("CONFIRMER",
              style: TextStyle(
                  fontFamily: 'BurnTheWitch'
              ),),
            style: ElevatedButton.styleFrom(
                primary: Colors.black
            ),
            onPressed: () {
              _onConfirm(context);

            },
          ),
        ),
        body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields(){
    return SizedBox(
      height: double.infinity,
      //padding: const EdgeInsets.all(20),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: ListView(
            children: [
              TextField(
                controller: nomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom de l\'Artiste'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              TextField(
                controller: descriptionController,
                textInputAction: TextInputAction.done,
                decoration: const InputDecoration(
                    label: Text('Description'),
                    prefixIcon: Icon(Icons.align_horizontal_left)
                ),
              ),
              const Text("Style de musique"),
              ListView.builder(
                itemCount: styleArtiste.length,
                itemBuilder: (BuildContext context, int index){
                  return Card(
                    child: ListTile(
                      leading: const Icon(Icons.album),
                      title: Text(
                        styleArtiste[index].nom
                      ),
                    ),
                  );
                },
              ),
              Autocomplete<String>(
                optionsBuilder: (TextEditingValue textEditingValue) {
                  if (textEditingValue.text == '') {
                    return const Iterable<String>.empty();
                  }
                  try {
                    late List<String> listNom = [];
                    listStyle.where((style) =>
                        style.nom.contains(textEditingValue.text))
                        .forEach((element) {
                      listNom.add(element.nom);
                    });
                    return listNom;
                  } on SocketException {
                    //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
                    SnackBar snackBarFailure = const SnackBar(content: Text(
                        "Nous avons des difficultés à joindre le serveur"));
                    ScaffoldMessenger.of(context).showSnackBar(
                        snackBarFailure);
                    return const Iterable<String>.empty();
                  }
                },
                onSelected: (String selection) {
                  nomStyle = selection;
                },
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.black
                ),
                onPressed: () => addStyle(),
                child: const Text(
                    'Ajouter le style de musique',
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'BurnTheWitch'
                    )
                  )
              )
            ],
          ),
        ),
      ),
    );
  }
}