import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../../Services/api_service.dart';
import '../../../models/artiste.dart';


class EditArtiste extends StatefulWidget {
  final Artiste artiste;

  const EditArtiste({required this.artiste});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<EditArtiste> {
  // This is  for form validations
  final formKey = GlobalKey<FormState>();

  // This is for text onChange
  TextEditingController NomController = TextEditingController();
  TextEditingController DescriptionController = TextEditingController();


  // Http post request
  Future editArtiste() async {
    //Récupération des champs
    //Préparation de la requête à énvoyer au serveur

    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Artiste/updateArtiste.php"),
          body: {
            "id": widget.artiste.id.toString(),
            "Nom": NomController.text,
            "Description": DescriptionController.text,
          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        NomController = TextEditingController(text: widget.artiste.nom);
        DescriptionController = TextEditingController(text: widget.artiste.description);

      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  void _onConfirm(context) async {
    await editArtiste();
  }

  @override
  void initState() {
    NomController = TextEditingController(text: widget.artiste.nom);
    DescriptionController = TextEditingController(text: widget.artiste.description);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Edit Scene",
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
          backgroundColor: Colors.black,

        ),
        bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            child: const Text('CONFIRM'),
            onPressed: () {
              _onConfirm(context);
            },
          ),
        ),
        body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields(){
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              const Spacer(),
              TextField(
                controller: NomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
              TextField(
                controller: DescriptionController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom d\'utilisateur'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}