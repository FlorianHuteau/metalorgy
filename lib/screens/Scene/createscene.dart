import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../../Services/api_service.dart';

class CreateScene extends StatefulWidget{

  const CreateScene({Key? key, Function? refreshSceneList, required this.idEvent}) : super(key: key);

  final int idEvent;

  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<CreateScene> {
  // Required for form validations
  final formKey = GlobalKey<FormState>();

  // Handles text onchange
  TextEditingController NomController = TextEditingController();
  TextEditingController NumeroController = TextEditingController();


  // Http post request to create new data
  Future _createScene() async {
    try{
      var responseRegister = await http.post(
          Uri.parse("${Env.URL_PREFIX}/Scene/createScene.php"),
          body: {
            "NomScene": NomController.text,
            "NScene": NumeroController.text,
            "id_Event":widget.idEvent.toString()
          }
      );

      if(responseRegister.statusCode == 200){
        //Informer l'utilisateur du succès de la requête
        SnackBar snackBarSuccess  = const SnackBar(content: Text("Enregistrement réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        //Vider nos TextField
        NomController.clear();
        NumeroController.clear();
      } else  if (responseRegister.statusCode >0){
        //Si le serveur répond autre chose que 200 alors on affiche le status
        SnackBar snackBarFailure  = SnackBar(content: Text("erreur : " + responseRegister.statusCode.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
      }
    } on SocketException {
      //On affiche un message lorsque le serveur est inatteignable (erreur de connexion
      SnackBar snackBarFailure = const SnackBar(content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarFailure);
    }
  }

  void _onConfirm(context) async {
    await _createScene();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Create Scene",
            style: TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
          backgroundColor: Colors.black,

        ),
        bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            child: Text("confirmer".toUpperCase(),
              style: const TextStyle(
                fontFamily: 'BurnTheWitch'
            ),),
            style: ElevatedButton.styleFrom(
                primary: Colors.black
            ),
            onPressed: () {
              _onConfirm(context);

            },
          ),
        ),
        body: _buildColumnFields()
    );
  }

  Widget _buildColumnFields(){
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            children: [
              TextField(
                controller: NomController,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(
                    label: Text('Nom de la scene'),
                    prefixIcon: Icon(Icons.person)
                ),
              ),

              TextField(
                controller: NumeroController,
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                decoration: const InputDecoration(
                    label: Text('Numero de la scene'),
                    prefixIcon: Icon(Icons.align_horizontal_left)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}