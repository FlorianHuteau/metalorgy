import 'package:flutter/material.dart';
import 'package:metalorgy/screens/Artiste/listeartiste.dart';
import 'package:metalorgy/screens/home.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MetalOrgy',
        initialRoute: '/',
        routes: <String, WidgetBuilder>{
          '/': (context) => const HomePage(admin: false),
          '/Artiste': (context) => const ListeArtiste(admin: true),
        },
    );
  }
  static void displayDialog(context, title, text) => showDialog(
    context: context,
    builder: (context) =>
        AlertDialog(
            title: Text(title),
            content: Text(text)
        ),
  );
}