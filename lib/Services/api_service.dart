import 'package:encrypt/encrypt.dart' as EncryptPack;
import 'package:crypto/crypto.dart' as CryptoPack;
import 'dart:convert' as ConvertPack;

class Env {
  static String URL_PREFIX = "http://192.168.1.41/flutter_api";
  static String keyCrypt = 'bnij8#PQtQGKcpGmoJRzJt!J&m@eSqbz';
  static String IVCript =  '9YSRSbFires4oPf#';

  static String chiffrageFunction(String string, bool chiffrage){
    var iv = CryptoPack.sha256.convert(ConvertPack.utf8.encode(IVCript)).toString().substring(0, 16);         // Consider the first 16 bytes of all 64 bytes
    var key = CryptoPack.sha256.convert(ConvertPack.utf8.encode(keyCrypt)).toString().substring(0, 32);       // Consider the first 32 bytes of all 64 bytes
    EncryptPack.IV ivObj = EncryptPack.IV.fromUtf8(iv);
    EncryptPack.Key keyObj = EncryptPack.Key.fromUtf8(key);
    final encrypter = EncryptPack.Encrypter(EncryptPack.AES(keyObj, mode: EncryptPack.AESMode.cbc));        // Apply CBC mode
    String result;
    if(chiffrage) {
      result = encrypter.encrypt(string, iv: ivObj).base64;
    } else{
      result = encrypter.decrypt(EncryptPack.Encrypted.from64(string), iv: ivObj);
    }
    return result;
  }
}