class Style{
  final int id;
  final String nom;
  final String description;

  Style({required this.id, required this.nom, required this.description});

  factory Style.fromJson(Map<String, dynamic> json){
    return Style(id: json['idStyle'], nom: json['NomStyle'], description: json['DescriptionStyle']);
  }

  Map<String, dynamic> toJson()=>{
    'NomStyle': nom,
    'DescriptionStyle': description
  };
}