class Event{
  final int id;
  final String nom;
  final DateTime dateDebut;
  final DateTime dateFin;
  final String description;
  final String adresse;

  Event({required this.id, required this.nom, required this.dateDebut, required this.dateFin, required this.description, required this.adresse });

  factory Event.fromJson(Map<String, dynamic> json){
    return Event(
      id: json['idEvent'] as int,
      nom: json['NomEvent'] as String,
      dateDebut: DateTime.parse(json['DateDebEvent'] as String),
      dateFin: DateTime.parse(json['DateFinEvent'] as String),
      description: json['DescriptionEvent'] as String,
      adresse: json['AdresseEvent'] as String
    );
  }

  Map<String, dynamic> toJson() =>{
    'NomEvent': nom,
    'DateDebEvent': dateDebut,
    'DateFinEvent': dateFin,
    'DescriptionEvent': description,
    'AdresseEvent': adresse
  };
}