class Artiste{
  final int id;
  final String nom;
  final String description;

  Artiste({required this.id, required this.nom, required this.description});

  factory Artiste.fromJson(Map<String, dynamic> json){
    return Artiste(
        id: json['idArtiste'] as int,
        nom: json['NomArtiste'] as String,
        description: json['DescriptionArtiste'] as String,
    );
  }

  Map<String, dynamic> toJson() =>{
    'NomArtiste' : nom,
    'DescriptionArtiste' : description,
  };
}