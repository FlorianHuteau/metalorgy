import './event.dart';
import './artiste.dart';
import './scene.dart';

class Programmation{
  final Event event;
  final Artiste artiste;
  final Scene scene;
  //final int numProg;
  final String heure;
  final String date;

  Programmation({required this.event, required this.artiste, required this.scene,  required this.heure, required this.date});

  factory Programmation.fromJson(Map<String, dynamic> json, Event event, Artiste artiste, Scene scene){
    return Programmation(event: event, artiste: artiste, scene: scene,  heure: json['HeureProg'], date: json['DateProg']);
  }

  Map<String, dynamic> toJson() =>{
    'id_Event': event.id,
    'id_Artiste': artiste.id,
    'id_Scene': scene.id,
    'HeureProg': heure,
    'DateProg':date
  };



}
