class Scene{
  final int id;
  final String nom;
  final int numero;
  final int idEvent;

  Scene({required this.id, required this.nom, required this.numero, required this.idEvent});
  
  factory Scene.fromJson(Map<String, dynamic> json){
    return Scene(id: json['idScene'], nom: json['NomScene'], numero: json['NScene'], idEvent: json['id_Event']);
  }

  Map<String, dynamic> toJson()=>{
    'NomScene': nom,
    'NScene' : numero,
    'idEvent': idEvent
  };
}