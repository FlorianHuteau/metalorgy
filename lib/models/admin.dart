import 'package:metalorgy/models/user.dart';

class Admin extends User{
  Admin(int idAdmin, String NomAdmin, String PrenomAdmin, String MailAdmin, String PasswordAdmin) :
        super(id: idAdmin, nom: NomAdmin, prenom: PrenomAdmin, mail: MailAdmin, password: PasswordAdmin);
}