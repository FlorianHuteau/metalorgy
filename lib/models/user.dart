class User {
  final int id;
  final String nom;
  final String prenom;
  final String mail;
  final String password;

  User({required this.id, required this.nom, required this.prenom, required this.mail, required this.password});

  factory User.fromJson(Map<String, dynamic> json){
    return User(
      id: json['idUser'] as int,
      nom:json['NomUser'] as String,
      prenom: json['PrenomUser'] as String,
      mail: json['MailUser'] as String,
      password: json['PasswordUser'] as String,
    );
  }

  Map<String, dynamic> toJson() =>{
    'NomUser': nom,
    'PrenomUser': prenom,
    'MailUser' : mail,
    'Password' : password
  };
}