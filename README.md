# MetalOrgy

Projet personnel d'application Flutter de gestion de festival.
L'application permet de créer des évènements et de gérer leurs programmations.
Les artistes sont consultable ainsi que les styles musiquaux qui leurs sont liés.
Les styles d'un évènement dépendent des styles des artistes qui compose la programmation.
L'application communique avec une base de donnée grâce à une API PHP.

## Fonctionnalité future :

- Pouvoir trier les artiste et les évènements par style.
- Ajouter un système de salle qui peuvent héberger différents évènements.
- Pouvoir trier les évènements par situation géographique.
- Pouvoir voir les éditions précédentes d'un évènement, ainsi que sa programmation.
- Pouvoir voir les évènements auquels participe un artiste depuis sa fiche.
